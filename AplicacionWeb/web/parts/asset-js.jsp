<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- jQuery 2.2.3 -->
<script src="<c:url value="plugins/jQuery/jquery-2.2.3.min.js"/>"></script>
<script src="dist/js/jquery.inputmask.bundle.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<c:url value="bootstrap/js/bootstrap.min.js"/>"></script>
<!-- SlimScroll -->
<script src="<c:url value="plugins/slimScroll/jquery.slimscroll.min.js"/>"></script>
<!-- FastClick -->
<script src="<c:url value="plugins/fastclick/fastclick.js"/>"></script>
<!-- AdminLTE App -->
<script src="<c:url value="dist/js/app.min.js"/>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="<c:url value="dist/js/miJavaScript.js"/>"></script>
<script src="<c:url value="dist/js/moment.min.js"/>"></script>
<script src="<c:url value="dist/js/bootstrap-datetimepicker.min.js"/>"></script>


