<footer class="main-footer">
    <div class="container">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0
        </div>
        <strong>Joan Nieto &copy; 2014-2016 <a href="#">666 Studios</a>.</strong> All rights
        reserved.
    </div>
    <!-- /.container -->
</footer>F