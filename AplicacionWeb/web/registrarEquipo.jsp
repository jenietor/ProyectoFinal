<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro de equipo</title>
        <c:import url="parts/asset-css.jsp"></c:import>
        </head>
        <body class="hold-transition skin-green layout-top-nav">
            <div class="wrapper">
            <c:import url="parts/navbar.jsp"></c:import>
            </div>
            <section class="content-header">
                <h1>
                    Simple Tables
                    <small>preview of simple tables</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="#">Tables</a></li>
                    <li class="active">Simple</li>
                </ol>
            </section>
            <section class="content">
                <div class="row">
                    <div class="col-xs-offset-1 col-md-4">
                        <section class="content-header">
                            <h1>
                                Registra Tu Equipo
                            </h1>
                            <ol class="breadcrumb">
                                <li><a href="paginaPrincipal.jsp"><i class="fa fa-dashboard"></i>Inicio</a></li>
                                <li><a href="Equipos.jsp">Ver Equipos</a></li>
                            </ol>
                        </section>
                        <div class="box box-primary">
                            <div class="box-header with-border">

                            </div>
                            <form role="form">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="nombreEquipo">Nombre del equipo</label>
                                        <input type="text" class="form-control" id="nombreEquipo" placeholder="Nombre del equipo" name="nombreEquipo">
                                    </div>
                                    <div class="form-group">
                                        <label>Modalidad</label>
                                        <select class="form-control">
                                            <option>Modalidad</option>
                                            <option value="futbol5">Futbol 5</option>
                                            <option value="futbol7">Futbol 7</option>
                                            <option value="futbol8">Futbol 8</option>
                                            <option value="futbol11">Futbol 11</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="diasParaJugar">Que dias quieren jugar</label>
                                        Selecciona los dias que te gustaria jugar
                                        <select class="js-example-basic-multiple js-states form-control" id="diasParaJugar" multiple="multiple">
                                            <option value="todos">Todos los dias</option>
                                            <option value="lunes">Lunes</option>
                                            <option value="martes">Martes</option>
                                            <option value="miercoles">Miercoles</option>
                                            <option value="jueves">Jueves</option>
                                            <option value="viernes">Viernes</option>
                                            <option value="sabado">Sabado</option>
                                            <option value="domingo">Domingo</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group date" id="fecha">
                                            <input type="text" class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="genero" id="masculino" value="masculino">
                                                Masculino
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="genero" id="femenino" value="femenino">
                                                Femenino
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputFile">Escudo del equipo</label>
                                            <input type="file" id="escudo">
                                            <p class="help-block">Carga o escoge el escudo de tu equipo</p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Registrar</button>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="col-sm-4">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>01</h3>
                                    <p>Nombre</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="registrarNuevoJugador.jsp" class="small-box-footer">
                                    Ver jugador <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>02</h3>

                                    <p>Nombre</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="registrarNuevoJugador.jsp" class="small-box-footer">
                                    Ver jugador <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>03</h3>
                                    <p>Nombre</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="registrarNuevoJugador.jsp" class="small-box-footer">
                                    Ver jugador <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>04</h3>
                                    <p>Nombre</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="registrarNuevoJugador.jsp" class="small-box-footer">
                                    Ver jugador <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>05</h3>
                                    <p>Nombre</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="registrarNuevoJugador.jsp" class="small-box-footer">
                                    Ver jugador <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <c:import url="parts/asset-js.jsp"></c:import>
    </body>
</html>
