<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <c:import url="parts/asset-css.jsp"></c:import>
            <title>Registrar Nuevo Jugador</title>
        </head>
        <body class="hold-transition skin-green layout-top-nav">
            <div class="wrapper">
            <c:import url="parts/navbar.jsp"></c:import>
            </div>
            <section class="content">
                <div class="row">
                    <div class="col-xs-offset-1 col-md-4">
                        <section class="content-header">
                            <h1>
                                Registro de Jugador
                            </h1>
                            <ol class="breadcrumb">
                                <li><a href="paginaPrincipal.jsp"><i class="fa fa-dashboard"></i>Inicio</a></li>
                            </ol>
                        </section>
                        <div class="box box-primary">
                            <div class="box-header with-border">

                            </div>
                            <form role="form">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="nombreJugador">Nombre del jugador</label>
                                        <input type="text" class="form-control" id="nombreJugador" placeholder="Nombre del jugador" name="nombreJugador">
                                    </div>
                                    <div class="form-group">
                                        <label for="cedula">Cedula</label>
                                        <input type="text" class="form-control" id="cedula" placeholder="Cedula" name="cedula">
                                    </div>

                                    <div class="form-group">
                                        <label for="correo">Correo Electronico</label>
                                        <input type="email" class="form-control" id="correo" placeholder="Correo electronico" name="correo">
                                    </div>
                                    <div class="form-group has-feedback">
                                        <input type="text" class="form-control fecha" placeholder="Año-Mes-Dia" name="fechaNacimiento">
                                        <span class="fa fa-calendar-o form-control-feedback" aria-hidden="true" ></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="IMAGENpERFIL">Foto de perfil</label>
                                        <input type="file" id="foto">
                                        <p class="help-block">Carga la foto que quieres para tu perfil</p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-sm-4">
                            <!-- small box -->
                            <div class="small-box bg-blue-active">
                                <div class="inner">
                                    <h3>01</h3>
                                    <p>Nombre</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="registrarNuevoJugador.jsp" class="small-box-footer">
                                    Ver jugador <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>02</h3>

                                    <p>Nombre</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="registrarNuevoJugador.jsp" class="small-box-footer">
                                    Ver jugador <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>03</h3>
                                    <p>Nombre</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="registrarNuevoJugador.jsp" class="small-box-footer">
                                    Ver jugador <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>04</h3>
                                    <p>Nombre</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="registrarNuevoJugador.jsp" class="small-box-footer">
                                    Ver jugador <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>05</h3>
                                    <p>Nombre</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="registrarNuevoJugador.jsp" class="small-box-footer">
                                    Ver jugador <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <c:import url="parts/asset-js.jsp"></c:import>
    </body>
</html>
