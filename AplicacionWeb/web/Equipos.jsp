<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Equipos</title>
        <c:import url="parts/asset-css.jsp"></c:import>
        </head>
        <body class="hold-transition skin-green-light layout-top-nav">
            <div class="wrapper">
            <c:import url="parts/navbar.jsp"></c:import>
        </div>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Equipos</h3>

                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="buscar">

                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody><tr>
                            <th>Nombre</th>
                            <th>Disponibilidad</th>
                            <th>Estado</th>
                            <th>Modalidad</th>
                            <th>ubicacion</th>
                        </tr>
                        <tr>
                            <td>John Doe</td>
                            <td>11-7-2014</td>
                            <td><span class="label label-success">Activo</span></td>
                            <td>futbol 5, futbol 8, futbol 11</td>
                            <td>Tunal</td>
                        </tr>
                        <tr>
                            <td>Alexander Pierce</td>
                            <td>11-7-2014</td>
                            <td><span class="label label-warning">Privado</span></td>
                            <td>futbol 11</td>
                            <td>Los rosales</td>
                        </tr>
                        <tr>
                            <td>Bob Doe</td>
                            <td>11-7-2014</td>
                            <td><span class="label label-primary">Play</span></td>
                            <td>Futbol 5, futbol 7, futbol 8</td>
                            <td>Chapinero</td>
                        </tr>
                        <tr>
                            <td>Mike Doe</td>
                            <td>11-7-2014</td>
                            <td><span class="label label-danger">Inactivo</span></td>
                            <td>Futbol 5, futbol 11</td>
                            <td>Bosa</td>
                        </tr>
                    </tbody></table>
            </div>
            <!-- /.box-body -->
        </div>
    </body>
</html>
