<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <c:import url="parts/asset-css.jsp"></c:import>
            <title>Registro</title>
        </head>
        <body class="hold-transition register-page" id="body">
            <div class="register-box">
                <div class="register-logo" id="titulo">
                    <a href="../../index2.html"><b>Aplicaciones</b>WEB</a>
                </div>

                <div class="register-box-body" id="box">
                    <p class="login-box-msg">Registrar un nuevo usuario</p>

                    <form action="<c:url value="paginaPrincipal.jsp"/>" method="post">
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Nombres" name="nombres" id="nombres">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Apellidos" name="apellidos" id="apellidos">
                        <span class="fa fa-info form-control-feedback" aria-hidden="true"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" placeholder="Email">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Confirmar password">
                        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="tel" class="form-control telefono"  placeholder="Telefono" name="telefono">
                        <span class="fa fa-phone form-control-feedback" aria-hidden="true"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control fecha" placeholder="Año-Mes-Dia" name="fechaNacimiento">
                        <span class="fa fa-calendar-o form-control-feedback" aria-hidden="true" ></span>
                    </div>
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Registrarse</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
            </div>
        </div>
        <c:import url="parts/asset-js.jsp"></c:import>
    </body>
</html>
