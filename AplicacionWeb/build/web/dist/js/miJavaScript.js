$(document).ready(function () {
    $(".fecha").inputmask('yyyy-mm-dd');
    $(".telefono").inputmask("99-9999999");
    $("#diasParaJugar").select2();
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('<i class="fa fa-angle-down"></i>');
    $(".hora").inputmask('h:s');
      $('#fecha').datetimepicker({
                    format: "LT"
                });

});

