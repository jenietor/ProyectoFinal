<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <c:import url="parts/asset-css.jsp"></c:import>
            <title>Ingresar</title>
        </head>
        <body class="hold-transition login-page" id="body">
            <div class="login-box">
                <div class="login-logo">
                    <a href="../../index2.html"><b>Aplicacion</b>WEB</a>
                </div>
                <!-- /.login-logo --> 
                <div class="login-box-body" id="box">
                    <p class="login-box-msg">Ingresa a tu perfil</p>

                    <form action="<c:url value="paginaPrincipal.jsp"/>" method="post">
                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" placeholder="Email">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">

                        </div>

                        <!-- /.col -->
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Iniciar</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
                <a href="registrar.jsp" class="text-center">Registrarse</a>

            </div>
            <!-- /.login-box-body -->
        </div>
        <c:import url="parts/asset-js.jsp"></c:import>
    </body>
</html>
